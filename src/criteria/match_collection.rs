

#[derive(PartialEq)]
pub struct MatchCollection<T> {
    items: Vec<T>
}

impl <T: PartialEq + Clone>MatchCollection<T> {
    pub fn new(items: Vec<T>) -> Self {
        MatchCollection{items: items}
    }

    pub fn match_percent(&self, other: &MatchCollection<T>) -> f64 {  
        if self == other {return 1.0}
        let intersection: Vec<T> = self.items.iter()
            .filter(|n| other.items.contains(&n))
            .cloned()
            .collect();
        if intersection.len() != 0 {
            intersection.len() as f64 / self.items.len() as f64
        } else {
            0.0
        }
    }
}

#[cfg(test)]
mod test_match_collection {
    use super::*;

    #[test]
    fn given_a_collection_of_strings_verify_matches() {
        let params = vec![
            (1.0, vec![], vec![]),
            (0.0, vec!["Java"], vec![]),
            (0.0, vec!["Java"], vec!["Python"]),
            (0.5, vec!["Haskell", "C#"], vec!["C#"]),
            (0.75, vec!["SOLID", "TDD", "Clean Architecture", "Simple Design"], vec!["SOLID", "TDD", "Simple Design"]),
            (0.25, vec!["Pair", "Project", "Mob", "Team"], vec!["Pair"]),
        ];

        for param in params {
            let (expected, uut_args, other_args) = param; 
            let uut = MatchCollection::new(uut_args);
            let other = MatchCollection::new(other_args);
            let result = uut.match_percent(&other);
            assert_eq!(expected, result);
        }
    }
}