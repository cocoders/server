use std::f64;

type TimeZone = f64;
const EMPTY:TimeZone = -13.0;
const VALID_TIME_ZONES: [TimeZone; 38] = [
    -12.0, -11.0, -10.0, -9.5, -9.0, -8.0, -7.0, 
    -6.0, -5.0, -4.0, -3.5, -3.0, -2.0, -1.0, 0.0, 
    1.0, 2.0, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 5.75, 
    6.0, 6.5, 7.0, 8.0, 8.75, 9.0, 9.5, 10.0, 10.5, 
    11.0, 12.0, 12.75, 13.0, 14.0
];


fn time_zone_is_valid(tz: TimeZone) -> bool {
    VALID_TIME_ZONES.contains(&tz)
}

fn get_time_zone_index(tz: TimeZone) -> Option<usize> {
    VALID_TIME_ZONES.iter().position(|&t| t == tz)
}

#[derive(Debug, PartialEq)]
pub struct TimeZoneRange {
    min: TimeZone,
    max: TimeZone,
}

impl TimeZoneRange {
    pub fn new(min: TimeZone, max: TimeZone) -> Self {
        TimeZoneRange{min: min, max: max}
    }

    pub fn empty() -> Self {
        TimeZoneRange{min: EMPTY, max: EMPTY}
    }

    pub fn is_empty(&self) -> bool {
        self.min == EMPTY && self.max == EMPTY
    }

    pub fn match_percent_for_each(&self, time_zones_ranges: &Vec<TimeZoneRange>) -> Vec<f64> {
        time_zones_ranges.iter()
            .map(| tzr | self.match_percent(tzr))
            .collect()
    }

    pub fn match_percent(&self, other: &TimeZoneRange) -> f64 {
        if self == other { return 1.0 }
        let self_time_zones = self.get_time_zones_in_range();
        let other_time_zones = other.get_time_zones_in_range();
        let intersection: Vec<TimeZone> = self_time_zones.iter()
            .filter(|tz| other_time_zones.contains(&tz))
            .cloned()
            .collect();
        if intersection.len() != 0 {
            intersection.len() as f64 / self_time_zones.len() as f64
        } else {
            0.0
        }
    }

    fn get_time_zones_in_range(&self) -> Vec<TimeZone> {
        if !time_zone_is_valid(self.min) || !time_zone_is_valid(self.max) { return vec![] }
        let start_index = get_time_zone_index(self.min).unwrap();
        VALID_TIME_ZONES[start_index..].iter()
            .chain(&VALID_TIME_ZONES[..start_index])
            .take_while(| tz | **tz != self.max )
            .chain(&[self.max])
            .cloned()
            .collect()
    }
}


#[cfg(test)]
mod test_time_zone_range {
    use super::*;

    #[test]
    fn given_two_time_zone_ranges_verify_matches() {
        let parameters = vec![
            (0.0, (-5.0, -5.0), (9.0, 9.0)),
            (0.0, (EMPTY, EMPTY), (0.0, 0.0)),
            (1.0, (EMPTY, EMPTY), (EMPTY, EMPTY)),
            (1.0, (0.0, 0.0), (0.0, 0.0)),
            (1.0, (0.0, 5.0), (0.0, 5.0)),
            (0.5, (0.0, 4.0), (2.0, 3.5)),
            (0.5, (-12.0, -1.0), (-6.0, 0.0)),
            (0.75, (-4.0, -2.0), (-3.5, -2.0)),
            (0.25, (10.0, -12.0), (14.0, -10.0)),
            (1.0, (9.0, 8.0), (-9.5, -10.0)),
        ];

        for param in parameters {
            let (expected, uut_args, other_args) = param;
            let uut = TimeZoneRange::new(uut_args.0, uut_args.1);
            let other= TimeZoneRange::new(other_args.0, other_args.1);
            let result = uut.match_percent(&other);
            assert_eq!(expected, result)
        }
    }

    #[test]
    fn given_a_list_of_time_zone_ranges_verify_different_match_results() {
        let uut = TimeZoneRange::new(6.0, 9.0);
        let time_zone_ranges = vec![
            TimeZoneRange::new(8.0, 9.0),
            TimeZoneRange::new(2.0, 1.0),
            TimeZoneRange::new(-5.0, -2.0)];
        let results = uut.match_percent_for_each(&time_zone_ranges);
        assert_eq!(vec![0.5, 1.0, 0.0], results);
    }
}